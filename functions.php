<?php
/**
 * Foundation_s functions and definitions
 *
 * @package Foundation_s
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'foundation_s_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function foundation_s_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Foundation_s, use a find and replace
	 * to change 'foundation-s' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'foundation-s', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 330, 210, true );
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'foundation-s' ),
		'topbar-l' => __( 'Topbar Left', 'foundation-s' ),
		'topbar-r' => __( 'Topbar Right', 'foundation-s' ),
		'offcanvas-l' => __( 'Topbar Right', 'foundation-s' ),
		'page-not-found' => __( '404 Navigation', 'foundation-s' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'foundation_s_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // foundation_s_setup
add_action( 'after_setup_theme', 'foundation_s_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function foundation_s_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'foundation-s' ),
		'id'            => 'sidebar-1',
		'description'   => 'The is the sidebar 1.',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'foundation-s' ),
		'id'            => 'sidebar-2',
		'description'   => 'The is the sidebar 2.',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar(array(
		'name' => 'FrontPage Twitter Widget',
		'id'   => 'twitter_widget',
		'description'   => 'Widget area for frontpage twitter feed.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5>',
		'after_title'   => '</h5>'
	) );
	register_sidebar(array(
		'name' => 'FrontPage About and Social',
		'id'   => 'frontpage_about_social',
		'description'   => 'Widget area for about and social follow.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5>',
		'after_title'   => '</h5>'
	) );
	register_sidebar(array(
		'name' => 'Left Outer Footer',
		'id'   => 'left_outer_footer',
		'description'   => 'Widget area for left outer footer',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5>',
		'after_title'   => '</h5>'
	) );
	register_sidebar(array(
		'name' => 'Left Inner Footer',
		'id'   => 'left_inner_footer',
		'description'   => 'Widget area for left inner footer',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5>',
		'after_title'   => '</h5>'
	) );
	register_sidebar(array(
		'name' => 'Right Inner Column',
		'id'   => 'right_inner_footer',
		'description'   => 'Widget area for right outer footer',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5>',
		'after_title'   => '</h5>'
	) );
	register_sidebar(array(
		'name' => 'Right Outer Column',
		'id'   => 'right_outer_footer',
		'description'   => 'Widget area for right outer footer',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5>',
		'after_title'   => '</h5>'
	) );
	


}
add_action( 'widgets_init', 'foundation_s_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function foundation_s_scripts() {
	wp_enqueue_style( 'foundation-s-style', get_stylesheet_uri() );

	wp_enqueue_style( 'foundation-normalize', get_stylesheet_directory_uri() . '/bower_components/foundation/css/normalize.css' );
    wp_enqueue_style( 'foundation', get_stylesheet_directory_uri() . '/bower_components/foundation/css/foundation.css' );
       
    /* Add Custom CSS */
    wp_enqueue_style( 'animate', get_stylesheet_directory_uri() . '/css/animate.min.css' );

    /* Add Custom CSS */
    wp_enqueue_style( 'foundation-s-custom-style', get_stylesheet_directory_uri() . '/dist/css/app.min.css' );

    /* Add Foundation JS */
	wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/bower_components/foundation/js/foundation.min.js', array( 'jquery' ), '1', true );
	wp_enqueue_script( 'foundation-modernizr-js', get_template_directory_uri() . '/bower_components/foundation/js/vendor/modernizr.js', array( 'jquery' ), '1', true );
	wp_enqueue_script( 'foundation-init', get_template_directory_uri() . '/dist/js/app.min.js', array( 'jquery' ), ’1′, true );
	 
	wp_enqueue_script( 'foundation-s-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'foundation-s-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'foundation_s_scripts' );

// class description_walker extends Walker_Nav_Menu
// {
//       function start_el(&$output, $item, $depth, $args)
//       {
//            global $wp_query;
//            $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

//            $class_names = $value = '';

//            $classes = empty( $item->classes ) ? array() : (array) $item->classes;

//            $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
//            $class_names = ' class="'. esc_attr( $class_names ) . '"';

//            $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

//            $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
//            $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
//            $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
//            $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
		   


//            $description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';



//             $item_output = $args->before;
//             $item_output .= '<a onclick="toggle(\'list\');"'.$attributes .'>';
//             $item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
//             $item_output .= $description.$args->link_after;
//             $item_output .= '</a>';
//             $item_output .= $args->after;

//             $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
//             }
// }





/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


