<?php
/**
 * Template Name: Homepage
 *
 * This is the template that diplayes the homepage.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Foundation_s
 */

get_header(); ?>

	<header id="masthead" class="frontpage-header" role="banner">
		  	<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('frontpage_about_social')) : else : ?>
				<p><strong>Twitter Widget</strong></p>
				<p>Please add the <strong><em>Rotating Tweets (Twitter widget and shortcode)</em></strong> widget to make this work.</p>
			<?php endif; ?> 
	</header><!-- #masthead -->
	<div class="twitter-feed">
		<div class="row">
		 	<div class="large-12 columns" id="name">
		 		<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('twitter_widget')) : else : ?>
					<p><strong>Twitter Widget</strong></p>
					<p>Please add the <strong><em>Rotating Tweets (Twitter widget and shortcode)</em></strong> widget to make this work.</p>
				<?php endif; ?>
		 	</div><!-- .large-12 .columns -->
		</div> <!-- .row -->
	</div>
	<div class="row" data-equalizer>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="large-8 columns" data-equalizer-watch>
			
			<?php // while ( have_posts() ) : the_post(); ?>

				<?php //get_template_part( 'content', 'page' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					// if ( comments_open() || '0' != get_comments_number() ) :
					// 	comments_template();
					// endif;
				?>

			<?php // endwhile; // end of the loop. ?>

			</div> <!-- .large-8 .columns -->
		</main><!-- #main -->

	</div><!-- #primary -->

<?php // get_sidebar(); ?>
<?php get_footer(); ?>
