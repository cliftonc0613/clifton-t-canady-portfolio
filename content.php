<?php
/**
 * @package Foundation_s
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>

		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php foundation_s_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			/* translators: %s: Name of current post */
			// the_content( sprintf(
			// 	__( '<p>Continue reading %s <span class="meta-nav">&rarr;</span></p>', 'foundation-s' ), 
			// 	the_title( '<span class="screen-reader-text">"', '"</span>', false )
			// ) );
			// the_excerpt( sprintf(
			// 	__( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'foundation-s' ), 
			// 	the_title( '<span class="screen-reader-text">"', '"</span>', true )
			// ) );
			echo wp_trim_words( get_the_content(), $num_words = 45, $more = null );

			//echo excerpt(25); ?
		?>

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'foundation-s' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search ?>
			<?php
				/* translators: used between list items, there is a space after the comma */
				$categories_list = get_the_category_list( __( ', ', 'foundation-s' ) );
				if ( $categories_list && foundation_s_categorized_blog() ) :
			?>
			<div class="cat-links">
				<?php //printf( __( 'Posted in %1$s', 'foundation-s' ), $categories_list ); ?>
			</div><br/>
			<?php endif; // End if categories ?>

			<?php
				/* translators: used between list items, there is a space after the comma */
				$tags_list = get_the_tag_list( '', __( ', ', 'foundation-s' ) );
				if ( $tags_list ) :
			?>
			<div class="tags-links">
				<?php //printf( __( 'Tagged: %1$s', 'foundation-s' ), $tags_list ); ?>
			</div><br/>
			<?php endif; // End if $tags_list ?>
		<?php endif; // End if 'post' == get_post_type() ?>

		<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
		<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'foundation-s' ), __( '1 Comment', 'foundation-s' ), __( '% Comments', 'foundation-s' ) ); ?></span>
		<?php endif; ?>

		<?php edit_post_link( __( 'Edit', 'foundation-s' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
