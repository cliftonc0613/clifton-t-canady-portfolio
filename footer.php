<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Foundation_s
 */
?>

</div><!-- #content -->
</div>
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="row">
			<div class="footer-widgets">
				<div class="small-12 medium-6 large-3 columns">
					<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('left_outer_footer')) : else : ?>
						<p><strong>Widget Ready</strong></p>
						<p>This left_column is widget ready! Add one in the admin panel.</p>
					<?php endif; ?>
				</div>
				<div class="small-12 medium-6 large-3 columns">
					<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('left_inner_footer')) : else : ?>
						<p><strong>Widget Ready</strong></p>
						<p>This left_column is widget ready! Add one in the admin panel.</p>
					<?php endif; ?>
				</div>
				<div class="small-12 medium-6 large-3 columns">
					<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('right_inner_footer')) : else : ?>
						<p><strong>Widget Ready</strong></p>
						<p>This left_column is widget ready! Add one in the admin panel.</p>
					<?php endif; ?>
				</div>
				<div class="small-12 medium-6 large-3 columns">
					<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('right_outer_footer')) : else : ?>
						<p><strong>Widget Ready</strong></p>
						<p>This left_column is widget ready! Add one in the admin panel.</p>
					<?php endif; ?>
				</div>
				<div class="site-info large-12 columns text-center">
					<?php printf( __( 'Designed By:  %1$s %2$s.', 'foundation-s' ), '', '<a href="http://ctwebdesignshop.com" rel="designer" target="_blank">CT Web Design Shop LLC</a>' ); ?>
				</div><!-- .site-info .large-12 .columns -->
			</div>
		</div> <!-- .row -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
</div>
</body>
</html>
