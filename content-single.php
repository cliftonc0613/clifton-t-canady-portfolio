<?php
/**
 * @package Foundation_s
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<div class="row">
		  <div class="medium-6 large-6 columns">
			<div class="entry-meta panel">
				<?php //foundation_s_posted_on(); ?>
				<p>POST TITLE: <?php the_title(); ?><br />
				AUTHOR: <?php the_author_link(); ?><br />
				POSTED: <?php the_time('jS F Y') ?><br />
				FILED AS: <?php the_category(', ') ?><br />
				COMMENT FEED: <?php comments_rss_link('RSS 2.0'); ?><br />
				PREVIOUS: <?php previous_post('%', '', 'yes', 'yes'); ?><br />
				NEXT: <?php next_post('%', '', 'yes', 'yes'); ?></p>
			</div><!-- .entry-meta -->
		  </div>
		  <div class="medium-6 large-6 columns">
		  	<div class="th"> 
			  	<!-- check if the post has a Post Thumbnail assigned to it. -->
				<?php if ( has_post_thumbnail() ) {
					the_post_thumbnail();
				} ?>
			</div>
		  </div>
		</div>
		
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'foundation-s' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php
			/* translators: used between list items, there is a space after the comma */
			$category_list = get_the_category_list( __( ', ', 'foundation-s' ) );

			/* translators: used between list items, there is a space after the comma */
			$tag_list = get_the_tag_list( '', __( ', ', 'foundation-s' ) );

			if ( ! foundation_s_categorized_blog() ) {
				// This blog only has 1 category so we just need to worry about tags in the meta text
				if ( '' != $tag_list ) {
					$meta_text = __( '', 'foundation-s' );
				} else {
					$meta_text = __( '', 'foundation-s' );
				}
			} else {
				// But this blog has loads of categories so we should probably display them here
				if ( '' != $tag_list ) {
					$meta_text = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" rel="bookmark">permalink</a>.', 'foundation-s' );
				} else {
					$meta_text = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" rel="bookmark">permalink</a>.', 'foundation-s' );
				}
			} // end check for categories on this blog

			printf(
				$meta_text,
				$category_list,
				$tag_list,
				get_permalink()
			);
		?>

		<?php edit_post_link( __( 'Edit', 'foundation-s' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
