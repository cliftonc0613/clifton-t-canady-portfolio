var gulp         = require('gulp'),
    sass         = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    rename       = require('gulp-rename'),
    cssmin       = require('gulp-cssmin'),
    jshint       = require('gulp-jshint'),
    concat       = require('gulp-concat'),
    uglify       = require('gulp-uglify'),
    imagemin     = require('gulp-imagemin'),
    pngcrush     = require('imagemin-pngcrush'),
    addsrc       = require('gulp-add-src'),
    watch        = require('gulp-watch'),
    livereload   = require('gulp-livereload'),
    rsync        = require('gulp-rsync'),
    scp          = require('gulp-scp');


gulp.task('sass', function() {
    gulp.src('./scss/app.scss')
        .pipe(sass())
        .pipe(autoprefixer("last 2 version", "ie 9"))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('img', function () {
    return gulp.src('./css/img/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngcrush()]
        }))
        .pipe(gulp.dest('./dist/img'));
});


gulp.task('js', function() {
    gulp.src('./js/app.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(addsrc('./js/*/*.js'))
        .pipe(concat('app.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./dist/js'));
});


gulp.task('watch', function() {
    livereload.listen();

    gulp.watch('./scss/**/*.scss', ['sass']).on('change', livereload.changed);
    //gulp.watch('./css/img/**/*', ['img']).on('change', livereload.changed);
    gulp.watch('./js/**/*.js', ['js']).on('change', livereload.changed);
    gulp.watch('./**/*.php').on('change', livereload.changed);
});

gulp.task('deploy', function() {
  gulp.src('/Users/cliftoncanady/Sites/cliftoncanady.co/wp-content/themes/*')
    .pipe(rsync({
      root: '/Users/cliftoncanady/Sites/cliftoncanady.co/',
      hostname: 'cliftoncanady.co',
      destination: '/var/www/wp-content/themes/'
    }));
});

// gulp.task('deploy', function () {
//     gulp.src('/Users/cliftoncanady/Sites/cliftoncanady.co/wp-content/themes/foundation-s')
//         .pipe(scp({
//             host: '104.131.12.216',
//             user: 'clifton',
//             port: 1313,
//             path: '/var/www/wp-content/themes/'
//         }));
// });

gulp.task('build', ['sass', 'img', 'js']);