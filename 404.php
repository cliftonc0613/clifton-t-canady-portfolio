<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package Foundation_s
 */

get_header(); ?>
<header id="masthead" class="site-header" role="banner">
    <div class="page-title">
    	<div class="row">
      		<div class="large-12 columns">
    			<h2 class="title right"><?php _e( 'Oops! That page can&rsquo;t be found.', 'foundation-s' ); ?></h2>
    		</div><!-- .large-12 .columns -->
      </div><!-- .row -->
    </div><!-- .page-title -->
</header><!-- #masthead -->
<div class="row">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
					
				<div class="page-content">
					<p><?php _e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'foundation-s' ); ?></p>

					<?php //get_search_form(); ?>

					<div class="small-12 medium-6 large-4 columns">
						<?php the_widget( 'WP_Widget_Recent_Posts' ); ?>
				    </div><!-- .small-12 .medium-6 .large-4 .columns-->

				    <div class="small-12 medium-6 large-4 columns">
				    	<h1>Navigation</h1>
						<?php wp_nav_menu( array( 'theme_location' => 'page-not-found', 'menu_class' => 'side-nav', ) ); ?>
				    </div><!-- .small-12 .medium-6 .large-4 .columns-->

				    <div class="small-12 medium-6 large-4 columns">
				    	<?php if ( foundation_s_categorized_blog() ) : // Only show the widget if site has multiple categories. ?>
				    	<div class="widget widget_categories">
						<h2 class="widget-title"><?php _e( 'Most Used Categories', 'foundation-s' ); ?></h2>
						<ul>
						<?php
							wp_list_categories( array(
								'orderby'    => 'count',
								'order'      => 'DESC',
								'show_count' => 1,
								'title_li'   => '',
								'number'     => 10,
							) );
						?>
						</ul>
					</div><!-- .widget -->
					<?php endif; ?>

					<?php
						/* translators: %1$s: smiley */
						$archive_content = '<p>' . sprintf( __( 'Try looking in the monthly archives.', 'foundation-s' ), convert_smilies( ':)' ) ) . '</p>';
						the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content" );
					?>

					<?php the_widget( 'WP_Widget_Tag_Cloud' ); ?>
				    </div><!-- .small-12 .medium-6 .large-4 .columns -->

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .row -->

<?php get_footer(); ?>
