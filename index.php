<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Foundation_s
 */

get_header(); ?>
	<header class="blog-site-header" role="banner">
	    <div class="blog-page-title">
	    	<div class="row">
	      		<div class="large-12 columns">
	    			<h1 class="title right"><?php echo get_the_title('89'); ?> </h1>
	    		</div><!-- .large-12 .columns -->
	      </div><!-- .row -->
	    </div><!-- .page-title -->
	</header><!-- #masthead -->
	<div class="row" data-equalizer>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="large-8 columns" data-equalizer-watch>
				
	 						
							<?php if ( have_posts() ) : ?>

									<ul class="medium-block-grid-2 large-block-grid-2">

								<?php /* Start the Loop */ ?>
								<?php while ( have_posts() ) : the_post(); ?>
						 				<li>
						 					<div class="post-wrapper">
						 				<a href="<?php the_permalink() ?>">

									<?php
										/* Include the Post-Format-specific template for the content.
										 * If you want to override this in a child theme, then include a file
										 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
										 */

											if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
												the_post_thumbnail();
											} 
										?>

										</a>
										
										<?php
										get_template_part( 'content', get_post_format() );
									?>
										</div>
									</li>
							    

								<?php endwhile; ?>
								</ul>
								<?php foundation_s_paging_nav(); ?>

							<?php else : ?>

								<?php get_template_part( 'content', 'none' ); ?>

							<?php endif; ?>
					
		    </div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
