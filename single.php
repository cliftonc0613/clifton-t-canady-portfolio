<?php
/**
 * The template for displaying all single posts.
 *
 * @package Foundation_s
 */

get_header(); ?>

<?php global $post; ?>
<?php
$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), false, '../css/img/clifton_refresh_yourself.jpg' );
//$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 2000,400 ), false, '../css/img/clifton_refresh_yourself.jpg' );
?>
<header id="masthead" class="site-header"  style="background: url(<?php echo $src[0]; ?> ) no-repeat center center fixed;-webkit-background-size: cover;
		  -moz-background-size: cover!important;
		  -o-background-size: cover!important;
		  background-size: cover!important;
		  padding: 2em 0!important;
		  margin: 0 0 25px 0;!important" role="banner">
	    <div class="page-title">
	    	<div class="row">
	      		<div class="large-12 columns">
	    			<?php the_title( '<h1 class="title right">', '</h1>' ); ?>
	    		</div><!-- .large-12 .columns -->
	      </div><!-- .row -->
	    </div><!-- .page-title -->
	</header><!-- #masthead -->
	<div class="row" data-equalizer>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			
				<div class="large-8 columns" data-equalizer-watch>
					<div class="site-content">
		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'single' ); ?>

			<?php foundation_s_post_nav(); ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || '0' != get_comments_number() ) :
					comments_template();
				endif;
			?>

		<?php endwhile; // end of the loop. ?>
			</div>
		</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>