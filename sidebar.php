<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Foundation_s
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div id="secondary" class="widget-area" role="complementary">
	<div class="large-4 columns">
		<div class="panel" data-equalizer-watch>
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
		</div><!-- .panel -->
	</div><!-- .large-4 .columns -->
</div><!-- #secondary -->
