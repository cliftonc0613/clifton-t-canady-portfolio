<?php
/**
 * Template Name: Resume
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Foundation_s
 */

get_header(); ?>
	<header id="masthead" class="site-header" role="banner">
	    <div class="page-title">
	    	<div class="row">
	      		<div class="large-12 columns">
	    			<?php the_title( '<h1 class="title right">', '</h1>' ); ?>
	    		</div><!-- .large-12 .columns -->
	      </div><!-- .row -->
	    </div><!-- .page-title -->
	</header><!-- #masthead -->
	<div class="row" data-equalizer>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="large-4 medium-4 columns" data-equalizer-watch>
                <div class="contact-info resume-box">
                    <div class="row header-bar">
                        <div class="small-1 medium-1 large-1 columns">
                           <i class="fa fa-user fa-2x"></i> 
                        </div>
                        <div class="small-11 medium-11 large-11 columns">
                            
                        </div>
                    </div>
                </div>    
			

			</div> <!-- .large-4 .medium-4  .columns -->
		</main><!-- #main -->

	</div><!-- #primary -->

<?php get_footer(); ?>
